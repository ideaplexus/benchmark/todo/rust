extern crate actix_web;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate futures;

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate pretty_env_logger;

use std::str::FromStr;
use actix_web::{middleware, web, App, HttpRequest, HttpResponse, HttpServer, Result};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum TodoState {
    Open,
    Closed,
}

#[derive(Debug)]
pub enum TodoStateError {
    ParsingError(String),
}

impl std::str::FromStr for TodoState {
    type Err = TodoStateError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_ref() {
            "open" => Ok(TodoState::Open),
            "closed" => Ok(TodoState::Closed),
            _ => Err(TodoStateError::ParsingError(format!("{}", s))),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Todo {
    pub id: u32,
    pub content: String,
    pub state: TodoState,
}


#[derive(Deserialize)]
pub struct ListParams {
    pub state: String,
}

pub fn list_tasks(filter: web::Query<ListParams>) -> Result<web::Json<Vec<Todo>>> {
    info!("list tasks query {:?}", filter.state);

    let state = TodoState::from_str(&filter.state).unwrap();
    info!("deserialized state into: {:?}", state);

    Ok(web::Json(vec![
        Todo {
            id: 1,
            content: "This is your first task".to_owned(),
            state: TodoState::Open,
        },
        Todo {
            id: 2,
            content: "This is your second task".to_owned(),
            state: TodoState::Open,
        },
    ]))
}

pub fn create_task(task: web::Json<Todo>) -> Result<web::Json<Todo>> {
    Ok(task)
}

pub fn get_task(id: web::Path<u32>) -> Result<web::Json<Todo>> {
    Ok(web::Json(Todo {
        id: id.into_inner(),
        content: "Buy milk".to_string(),
        state: TodoState::Open,
    }))
}

pub fn update_task(_req: HttpRequest) -> Result<web::Json<Todo>> {
    unimplemented!()
}

pub fn delete_task(_req: HttpRequest) -> Result<web::Json<Todo>> {
    unimplemented!()
}

pub fn health_check(_req: HttpRequest) -> HttpResponse {
    HttpResponse::NoContent().into()
}

fn main() {
    std::env::set_var("RUST_LOG", "actix_web=info,info");
    pretty_env_logger::init();

    let url = "127.0.0.1:3000";
    info!("Running server on {}", url);

    HttpServer::new(
        || App::new()
            .wrap(middleware::Logger::default())
            .service(web::resource("/healthz").route(web::get().to(health_check)))
            .service(web::scope("/tasks")
                .service(web::resource("")
                    .route(web::get().to(list_tasks))
                    .route(web::post().to(create_task))
                )
                .service(web::resource("/{task_id}")
                    .route(web::get().to(get_task))
                    .route(web::put().to(update_task))
                    .route(web::delete().to(delete_task))
                )
            )
    ).bind(url)
        .unwrap()
        .run();
}
